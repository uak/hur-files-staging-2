import toml
import os
import requests
import logging

# ~ logging.basicConfig(level=logging.DEBUG)

logger = logging.getLogger(__name__)
# ~ logger.setLevel(logging.DEBUG)



urls_file = os.environ['URL_FILE']
data_dir = os.environ['DATA_DIR']

def main(urls_file, data_dir):
# Read the TOML file
    logging.basicConfig(level=logging.DEBUG)
    
    count = 0
    
    with open(urls_file, 'r') as file:
        data = toml.load(file)
        logger.info("loading urls file")

    # Check if the completed_urls key exists, if not create it
    if 'completed_urls' not in data:
        data['completed_urls'] = []
        logger.info("Checked if completed_urls in data")
    else:
        completed_urls = data['completed_urls']
        urls = data['urls']
        logger.info(f"Number of completed urls: {len(completed_urls)}")
        logger.info(f"Number of urls: {len(urls)}")

    # Copy list
    url_list = data['urls'].copy()
    
    # Iterate through the urls and download them if they do not already exist
    for url in url_list:
        count += 1
        filename = url.split('/')[-1]
        response = requests.get(url)
        logger.info(f"Requested {url}")
        with open(f"{data_dir}{filename}", 'wb') as file:
            file.write(response.content)
            logger.info(f"saved {filename}")
        data['completed_urls'].append(url)
        logger.info(f"added {url} to list of completed url")
        data['urls'].remove(url)
        logger.info(f"Removed {url} from list of urls")
        logger.info(f"Count =  {count}")

    
    # Uniqe values only
    myset = set(data['completed_urls'])
    data['completed_urls'] = list(myset)
    
    # Sort values
    data['completed_urls'].sort()

    # Update the TOML file with the completed_urls list
    with open(urls_file, 'w') as file:
        toml.dump(data, file)
        logger.info(f"saved new data to urls.toml file")



if __name__ == "__main__":
    main(urls_file, data_dir)
